#!/bin/bash

set -e

#####################################################################
#
#   _   _   _      ____  _       _ _        _ 
#  | | | | / \    |  _ \(_) __ _(_) |_ __ _| |
#  | | | |/ _ \   | | | | |/ _` | | __/ _` | |
#  | |_| / ___ \  | |_| | | (_| | | || (_| | |
#   \___/_/   \_\ |____/|_|\__, |_|\__\__,_|_|
#                          |___/              
#
#
# This script builds the UA Digital Ubuntu development environment
#
#####################################################################


# update our disk image
apt-get -y update && apt-get -y upgrade

# install basic tools
apt-get -y install \
  vim \
  git \
  curl \
  php \
  php-xml \
  mysql-client

# install composer
curl -sS https://getcomposer.org/installer | php
mv composer.phar /usr/local/bin/composer

# install atom
curl -L https://atom.io/download/deb > atom.deb  
apt install ./atom.deb
rm ./atom.deb

# build user
curl -L https://bitbucket.org/kevcooper/udigitalbox/raw/master/build-user.sh > /home/uadigital/build-user.sh
chown uadigital:uadigital /home/uadigital/build-user.sh
chmod +x /home/uadigital/build-user.sh
sudo -u uadigital /home/uadigital/build-user.sh

