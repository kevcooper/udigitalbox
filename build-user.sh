#!/bin/bash

set -e

#####################################################################
#
#   _   _   _      ____  _       _ _        _ 
#  | | | | / \    |  _ \(_) __ _(_) |_ __ _| |
#  | | | |/ _ \   | | | | |/ _` | | __/ _` | |
#  | |_| / ___ \  | |_| | | (_| | | || (_| | |
#   \___/_/   \_\ |____/|_|\__, |_|\__\__,_|_|
#                          |___/              
#
#
# This script builds the UA Digital Ubuntu development environment
#
#####################################################################

# become root to update and install packages
sudo bash << EOF

# update our disk image
apt-get -y update && apt-get -y upgrade

# install basic tools
apt-get -y install \
  vim \
  git \
  curl \
  php \
  php-xml \
  mysql-client \
  filezilla

# clean up unused packages
apt autoremove

# install atom
curl -L https://atom.io/download/deb > atom.deb
apt-get install ./atom.deb
rm atom.deb

# install docker
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
apt-get update
apt-get install -y docker-ce

EOF

# add to .bashrc
cat >> $HOME/.bashrc << EOF

##############
# UA Digital #
##############
export PATH="\$HOME/.config/composer/vendor/bin:\$PATH"
EOF

# create default directories
mkdir $HOME/src

# clone ua digital repos
(
cd src
git clone https://bitbucket.org/ua_drupal/ua_quickstart.git
git clone https://bitbucket.org/ua_drupal/ua_bootstrap.git
git clone https://bitbucket.org/ua_drupal/ua_zen.git
)

# install composer
curl -sS https://getcomposer.org/installer | php
sudo mv composer.phar /usr/local/bin/composer

# install drush
composer global require drush/drush:7.*

# install terminus
composer global require pantheon-systems/terminus

# set launcher
gsettings set com.canonical.Unity.Launcher favorites "['application://org.gnome.Nautilus.desktop','application://gnome-terminal.desktop','application://atom.desktop','application://firefox.desktop']"

